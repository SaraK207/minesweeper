/*
SENG 513 Assignment 2
Lab B04
File Name: minesweeper.js
Author: Sara Khan
UCID: 30024461
Due Date: October 18, 2020

University of Calgary
*/


$.event.special.tap.emitTapOnTaphold = false;

$(function(){
    let rows = 8;         
    let columns = 10;      
    let totalCells = rows * columns;    
    let numOfBombs = 10;
    let bombArray = [];
    let gameBoard = [];
    let winCount;
    let checked = [];
    let timer = 0;
    let flagNum = numOfBombs;
    let gameEnd = false;


    //function to initialize game board by placing html for cells and assinging the correct size to cells
    function initBoard(totalCells){
        winCount = totalCells - numOfBombs;
        bombArray = [];
        checked = [];
        let rowNum = 0;
        timer = 0;
        flagNum = numOfBombs;
        gameEnd = false;
        $("#gameHeader").html("<p style=\"width: 25%;\">Minesweeper</p><div style=\"margin-left: 3px;width: 25%;justify-content: center;align-items: center;\" class=\"flexRow\"><img src=\"flag.png\" width=\"25\" height=\"auto\" style=\"margin-right: 10px;\"><p id=\"flagNum\">" + flagNum + "</p></div><div style=\"width: 25%;justify-content: center;align-items: center;\" class=\"flexRow\"><img src=\"timer.png\" width=\"35\" height=\"auto\"><p id=\"timer\" style=\"margin-left: 10px;\">" + timer + "</p></div><select name=\"difficulty\" id=\"difficulty\"><option value=\"Easy\">Easy</option><option value=\"Medium\">Medium</option><option value=\"Hard\">Hard</option></select>");
        if(rows  === 8){$("#difficulty").val("Easy")}
        else if(rows === 14){$("#difficulty").val("Medium")}
        else{$("#difficulty").val("Hard")}
        $("#gameContainer").html(" ");
        
        for(let i = 0; i < totalCells; i++){
            if(i%columns === 0){
                rowNum = i;
                $("#gameContainer").append("<div class=\"row flexRow\" id=\"r-" + rowNum + "\"><div class=\"cell firstclick flexRow\" id=\"" + i + "\"></div>");   
            }
            else if(i === columns - 1 || i%columns === columns-1){
                $("#r-" + rowNum).append("<div class=\"cell firstclick flexRow\" id=\"" + i + "\"></div></div>"); 
            }
            else{
                $("#r-" + rowNum).append("<div class=\"cell firstclick flexRow\" id=\"" + i + "\"></div>"); 
            }
            
        }

        let cellWidth = 100 / columns;
        $(".cell").css({"min-width":cellWidth + "%", "max-width":cellWidth + "%"});

        let rowHeight = 100 / rows;
        $(".row").css({"min-height":rowHeight + "%", "max-height":rowHeight + "%"});

        for(let i = 0; i < totalCells; i++){
            bombArray.push(0);   
        }

    }

    //initialize board
    initBoard(totalCells);



    //check for win here
    function checkWin(){
        if(winCount === 0){
            gameEnd = true;
            $("#endMessage").text("Winner!");
            $("#finalTime").html("<img src=\"timer.png\" width=\"35\" height=\"auto\" style=\"margin-right: 7px;\">" + timer);
            $("#gameOver").css("display", "flex");
        }
    }

    function checkFlag(id){
        if($("#" + id).hasClass("flag") && $("#" + id).hasClass("covered")){
            $("#" + id).removeClass("flag");
            $("#" + id).html(" ");
            flagNum++;
            $("#flagNum").text(flagNum);
        }
        else{
            if($("#" + id).hasClass("covered")){
                $("#" + id).addClass("flag");
                $("#" + id).html("<img src=\"flag.png\" width=\"40%\">");
                flagNum--;
                $("#flagNum").text(flagNum);
            }
        }
    }

    function recursiveReveal(curr){
        if(gameBoard[curr] !== 0){
            if($("#" + curr).hasClass("covered")){
                if(gameBoard[curr] === "B"){$("#" + curr).removeClass("covered").html("<img src=\"bomb.png\" width=\"60%\">");}
                else{
                    $("#" + curr).removeClass("covered").text(gameBoard[curr]);
                    if(gameBoard[curr] === 1){$("#" + curr).css("color", "#1976d2");}
                    else if(gameBoard[curr] === 2){$("#" + curr).css("color", "#388e3c");}
                    else if(gameBoard[curr] === 3){$("#" + curr).css("color", "#d32f2f");}
                    else if(gameBoard[curr] === 4){$("#" + curr).css("color", "#7b1fa2");}
                    else if(gameBoard[curr] === 5){$("#" + curr).css("color", "#ff8f00");}
                    else if(gameBoard[curr] === 6){$("#" + curr).css("color", "yellow");}
                    else if(gameBoard[curr] === 7){$("#" + curr).css("color", "gray");}
                    else if(gameBoard[curr] === 8){$("#" + curr).css("color", "black");}
                    else{$("#" + curr).css("color", "white");}

                    winCount--;
                    checkWin();
                }
            }
        }
        else{
            if(checked.includes(curr) === false){
            
                $("#" + curr).removeClass("covered").text(" ");
                winCount--;
                checkWin();
                checked.push(curr);
                
                if(curr === 0){ 
                    recursiveReveal(curr + 1);
                    recursiveReveal(curr + columns);
                    recursiveReveal(curr + columns + 1); 
                }
                else if(curr === columns - 1){
                    recursiveReveal(curr - 1);
                    recursiveReveal((curr + columns) - 1);
                    recursiveReveal(curr + columns);
                }
                else if(curr === (rows - 1) * columns){
                    recursiveReveal(curr + 1);
                    recursiveReveal(curr - columns);
                    recursiveReveal((curr - columns) + 1); 
                }
                else if(curr === (rows*columns) - 1){
                    recursiveReveal(curr - 1);
                    recursiveReveal((curr - columns) - 1);
                    recursiveReveal(curr - columns);
                }
                else if(curr%columns === 0){
                    recursiveReveal(curr - columns);
                    recursiveReveal((curr - columns) + 1);
                    recursiveReveal(curr + 1);
                    recursiveReveal(curr + columns);
                    recursiveReveal(curr + columns + 1);
                }
                else if(curr < columns){
                    recursiveReveal(curr - 1);
                    recursiveReveal((curr + columns) - 1);
                    recursiveReveal(curr + columns);
                    recursiveReveal(curr + 1);
                    recursiveReveal(curr + columns + 1);
                }
                else if(curr > (rows - 1)*columns){
                    recursiveReveal(curr - 1);
                    recursiveReveal((curr - columns) - 1);
                    recursiveReveal(curr - columns);
                    recursiveReveal(curr + 1);
                    recursiveReveal((curr - columns) + 1);
                }
                else if(curr%columns === columns-1){
                    recursiveReveal(curr - 1);
                    recursiveReveal((curr - columns) - 1);
                    recursiveReveal(curr - columns);
                    recursiveReveal((curr + columns) - 1);
                    recursiveReveal(curr + columns);
                }
                else{
                    recursiveReveal(curr + 1);
                    recursiveReveal(curr + columns);
                    recursiveReveal(curr + columns + 1);
                    recursiveReveal(curr - 1);
                    recursiveReveal((curr + columns) - 1);
                    recursiveReveal(curr - columns);
                    recursiveReveal((curr - columns) + 1);
                    recursiveReveal((curr - columns) - 1);
                }
            }
        }
    }


    $("#gameHeader").on("change", "#difficulty", function(){
        if($("#difficulty").val() === "Easy"){rows = 8; columns = 10; totalCells = rows * columns; numOfBombs = 10; initBoard(totalCells);}
        else if($("#difficulty").val() === "Medium"){rows = 14; columns = 18; totalCells = rows * columns; numOfBombs = 40; initBoard(totalCells);}
        else{rows = 20; columns = 24; totalCells = rows * columns; numOfBombs = 99; initBoard(totalCells);}
    })

    $( "#gameContainer").on( "tap", ".cell",  function() {
        if($(this).hasClass("covered") && $(this).hasClass("flag") === false){
            
            //Uncover the cell
            recursiveReveal(parseInt($(this).attr("id")));
            
            //check for bomb here
            if(gameBoard[parseInt($(this).attr("id"))] === "B"){
                for(let i = 0; i < gameBoard.length; i++){
                    if(gameBoard[i] === "B"){$("#" + i).removeClass("covered").html("<img src=\"bomb.png\" width=\"60%\">");}
                }
                gameEnd = true;
            $("#endMessage").text("Oh no! You found a Bomb :(");
            $("#finalTime").text(" ");
            $("#gameOver").css("display", "flex");
            }  
        }
        
        if($(this).hasClass("firstclick")){
            gameBoard = [];
            let counter = numOfBombs;

            //function to place bombs in random cells except first cell
            let placeBombs = function(fclick){
                let randNum = 0;
                let exclude = [fclick];
                if(fclick === 0){exclude.push(fclick + 1);exclude.push(fclick + columns);exclude.push(fclick + columns + 1);}
                else if(fclick === columns - 1){exclude.push(fclick - 1);exclude.push((fclick + columns) - 1);exclude.push(fclick + columns);}
                else if(fclick === (rows - 1) * columns){exclude.push(fclick + 1);exclude.push(fclick - columns);exclude.push((fclick - columns) + 1);}
                else if(fclick === (rows*columns) - 1){exclude.push(fclick - 1);exclude.push((fclick - columns) - 1);exclude.push(fclick - columns);}
                else if(fclick%columns === 0){exclude.push(fclick - columns);exclude.push((fclick - columns) + 1);exclude.push(fclick + 1);exclude.push(fclick + columns);exclude.push(fclick + columns + 1);}
                else if(fclick < columns){exclude.push(fclick - 1);exclude.push((fclick + columns) - 1);exclude.push(fclick + columns);exclude.push(fclick + 1);exclude.push(fclick + columns + 1);}
                else if(fclick > (rows - 1)*columns){exclude.push(fclick - 1);exclude.push((fclick - columns) - 1);exclude.push(fclick - columns);exclude.push(fclick + 1);exclude.push((fclick - columns) + 1);}
                else if(fclick%columns === columns-1){exclude.push(fclick - 1);exclude.push((fclick - columns) - 1);exclude.push(fclick - columns);exclude.push((fclick + columns) - 1);exclude.push(fclick + columns);}
                else{exclude.push(fclick + 1);exclude.push(fclick + columns);exclude.push(fclick + columns + 1);exclude.push(fclick - 1);exclude.push((fclick + columns) - 1);exclude.push(fclick - columns);exclude.push((fclick - columns) + 1);exclude.push((fclick - columns) - 1);}

                
                for(let i = 0; i < counter; i++){
                    randNum = Math.floor(Math.random() * totalCells);
                    //put a 1 if there is a bomb, remain 0 if there isn't
                
                    
                    if(exclude.includes(randNum) === false && bombArray[randNum] !== 1){
                        bombArray[randNum] = 1;
                        counter--;
                    }
                }
            }

            //call function to place bombs trying until all bombs have a valid position
            while(counter !== 0){
                placeBombs(parseInt($(this).attr("id")));
            }

            //this loop will calculate the number of bombs in adjacent cells for each cell not containing a bomb
            for(let i = 0; i < bombArray.length; i++){
                
                let adjBombs = 0;
                if(bombArray[i] !== 1){
                    if(i === 0){adjBombs += bombArray[i + 1] + bombArray[i + columns] + bombArray[i + columns + 1];}
                    else if(i === columns - 1){adjBombs += bombArray[i - 1] + bombArray[(i + columns) - 1] + bombArray[i + columns];}
                    else if(i === (rows - 1) * columns){adjBombs += bombArray[i + 1] + bombArray[i - columns] + bombArray[(i - columns) + 1];}
                    else if(i === (rows*columns) - 1){adjBombs += bombArray[i - 1] + bombArray[(i - columns) - 1] + bombArray[i - columns];}
                    else if(i%columns === 0){adjBombs += bombArray[i - columns] + bombArray[(i - columns) + 1] + bombArray[i + 1] + bombArray[i + columns] + bombArray[i + columns + 1];}
                    else if(i < columns){adjBombs += bombArray[i - 1] + bombArray[(i + columns) - 1] + bombArray[i + columns] + bombArray[i + 1] + bombArray[i + columns + 1];}
                    else if(i > (rows - 1)*columns){adjBombs += bombArray[i - 1] + bombArray[(i - columns) - 1] + bombArray[i - columns] + bombArray[i + 1] + bombArray[(i - columns) + 1];}
                    else if(i%columns === columns-1){adjBombs += bombArray[i - 1] + bombArray[(i - columns) - 1] + bombArray[i - columns] + bombArray[(i + columns) - 1] + bombArray[i + columns];}
                    else{adjBombs += bombArray[i + 1] + bombArray[i + columns] + bombArray[i + columns + 1] + bombArray[i - 1] + bombArray[(i + columns) - 1] + bombArray[i - columns] + bombArray[(i - columns) + 1] + bombArray[(i - columns) - 1];}
                    
                    gameBoard.push(adjBombs);
                    
                }
                else{gameBoard.push("B");}

                $("#"+ i).removeClass("firstclick").addClass("covered");
            }
            recursiveReveal(parseInt($(this).attr("id")));
        }
        
        
    });

    setInterval(function(){
        if(gameEnd === false){
            timer++;
            $("#timer").text(timer);
        } 
    }, 1000);

    $( "#gameContainer" ).on( "mousedown", ".cell", function(event) {
        if(event.which === 3){
            checkFlag($(this).attr("id"));
        }   
    })

    $( "#gameContainer" ).on( "taphold", ".cell", function() {
        checkFlag($(this).attr("id")); 
    })

        
    $("#playAgain").on("click", function(){
        $("#gameOver").hide();
    initBoard(totalCells);
    })    
          

})
